// Pour le clique sur le bouton résultat
document.getElementById("generation").addEventListener("click", function () {
    // Lecture du fichier
    lireFichierGeneration();
});

document.getElementById("verification").addEventListener("click", function () {
    // Lecture du fichier
    lireFichierVerification();
});

document.getElementById("insertion").addEventListener("click", function () {
    var val = parseInt(document.getElementById("insertion-val").value);
    if(verifierInput(val) && globalTABR != undefined) {
        globalTABR.inserer(val);
        document.getElementById("tabr").innerHTML = "<h5>TABR Généré</h5><br>" + globalTABR.toHTML();
    } else {
        alert("Pas de TABR ou Format de la valeur incorrect");
    }
});

document.getElementById("suppression").addEventListener("click", function () {
    var val = parseInt(document.getElementById("suppression-val").value);
    if(verifierInput(val) && globalTABR != undefined) {
        globalTABR.supprimer(val);
        if(globalTABR.tab[0] == null) {
            document.getElementById("tabr").innerHTML = "Pas de TABR généré";
            globalTABR = undefined;
        } else {
            document.getElementById("tabr").innerHTML = "<h5>TABR Généré</h5><br>" + globalTABR.toHTML();
        }
    } else {
        alert("Pas de TABR ou Format de la valeur incorrect");
    }
});

document.getElementById("fusion").addEventListener("click", function () {
    var val = parseInt(document.getElementById("fusion-val").value);
    if(verifierLigne(val) && globalTABR != undefined) {
        globalTABR.fusion(val-1);
        document.getElementById("tabr").innerHTML = "<h5>TABR Généré</h5><br>" + globalTABR.toHTML();
    } else {
        alert("Pas de TABR ou Format de la valeur incorrect (ligne > 0)");
    }
});

document.getElementById("genererAbr").addEventListener("click", function () {
    if(globalTABR != undefined) {
        var abr = globalTABR.toAbr();
        document.getElementById("abr").innerHTML = "<hr><h5>ABR Généré</h5><br>" + abr.toString();
    } else {
        alert("Pas de TABR");
    }
});

// Quand on charge un fichier différent
document.getElementById("fileInput").addEventListener("change", function () {
    globalTABR = undefined;
})


document.getElementById("genererFichier").addEventListener("click", function () {
    var val = document.getElementById("nomFichier").value;
    if (globalTABR != undefined) {
        if(val == "") {
            genererFichier("TABRico.txt", globalTABR.toString());
        } else {
            genererFichier(val+".txt", globalTABR.toString());
        }
    } else {
        alert("Veuillez générer un TABR valide");
    }

});