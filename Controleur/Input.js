function verifierInput(input) {
    return !isNaN(parseFloat(input)) && isFinite(input);
}

function verifierLigne(input) {
    return verifierInput(input) && input > 0;
}